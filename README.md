# React Dashboard

A simple Dashboard with nonesense data, just to test ReactJS with a reallife task.

## Tasks Done

* ~~Build front end using HTML, CSS and JQuery~~
* ~~Setup project using Create React App~~
* ~~Loop through data/array~~
* ~~Checkbox select~~
* ~~Checkbox select all~~
* ~~Add accordion functionality~~
* ~~Add Google Maps and markers~~

## Tasks to do

* Fix Checkbox issue when selecting/deslecting individual checbox and then use check all
* Add Firebase db for the data
* Add Sass compiler or standardise a way of using CSS with ReactJS (for personal preference)



