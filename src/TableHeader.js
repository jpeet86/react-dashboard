import React, { Component } from 'react';
import Cell from './Cell.js';

class TableHeader extends Component {

  render() {

    return (
      <div className="t-row-header t-header">
        <div className="column-first">
          <input 
            type="checkbox" 
            onChange={this.props.checkAll} 
            checked={this.props.isCheckedAll} />
        </div>
        <Cell content="User" />            
        <Cell content="Group" />            
        <Cell content="Last Active" />            
        <div className="column-last"></div>
      </div>
    );
  }
}

export default TableHeader;
