import React, { Component } from 'react';

class Cell extends Component {
  render() {
    return (
      <div className="column">{this.props.content}</div>
    )
  }
}

export default Cell;