import React, { Component } from 'react';
import Header from './TableHeader.js';

class Checkbox extends Component {

	constructor(props) {

		super(props);

		this.setState = {
			checked: false
		}

	}

	render() {
		return (
		  <input type="checkbox" checked={this.props.isChecked} />
		)
	}
}

export default Checkbox;