import React, { Component } from 'react';

import './App.css';
import TableRow from './TableRow.js';
import Cell from './Cell.js';
import TableHeader from './TableHeader.js';

class App extends Component {

	constructor(props) {
		super(props);

    	this.state = {
      		rows: [],
    		isCheckedAll: false,
    		isChecked: false
    	} 

    	this.checkSingle = this.checkSingle.bind(this);
    	this.checkAll = this.checkAll.bind(this);
	}

	checkSingle(i) {
		// Use index to change the specific row in rows.state
		this.state.rows[i] = !this.state.rows[i]
	}

	checkAll() {
		const rows = this.state.rows.slice(0);

		users.map((row, i) => {
			rows[i] = !this.state.isCheckedAll
		})

		this.setState({
			rows,
			isCheckedAll: !this.state.isCheckedAll
		})
	}

	componentDidMount() {
		// Set each row to false using users.map
		const rows = this.state.rows.slice(0);

		users.map((row, i) => {
			rows[i] = false
		})

		this.setState({
			rows
		})
	}

	render() {

		const tableRows = users.map((user, i) => 
			<TableRow 
				key={i}
				index={i}
				userName={user.name}
				userRole={user.role}
				userEmail={user.email}
				userLastActive={user.lastActive}
				userPhone={user.phone}
				userGroup={user.group}
				userPhoto={user.photo}
				userDevice={user.device}
				userDeviceAdded={user.deviceAdded}
				userToken={user.token}
				userLocation={user.location}
				mapLat={user.lat}
				mapLng={user.lng}
				checkSingle={this.checkSingle}
				checkedState={this.state.rows[i]} />
		)

		return (
			<div>
				<TableHeader 
					checkAll={this.checkAll} 
					checked={this.isCheckedAll}  />
				<div className="tbody">
		          {tableRows}   
		        </div>
			</div>
		);
	}

}

const users = [
  {
      "name":"Max A. Duran",
      "photo":"/img/sample/img-user.png",
      "role":"Coil taper",
      "email":"MaxADuran@teleworm.us",
      "group":"Administrators",
      "lastActive":"2017-01-14T21:40:30Z",
      "phone":"+1 815-789-8441",
      "location":"Orangeville, USA",
      "lng":"-119.003906",
      "lat":"42.940339",
      "device":"iOS App",
      "token":"6384472055",
      "deviceAdded":"2016-12-14"
  },
  {
      "name":"Johnny T. Shealy",
      "photo":"/img/sample/img-user.png",
      "role":"Metalworking machine operator",
      "email":"JohnnyTShealy@teleworm.us",
      "group":"Users",
      "lastActive":"2016-12-09T21:40:30Z",
      "phone":"+1 517-333-9029",
      "location":"East Lansing, USA",
      "lng":"-93.955078",
      "lat":"45.213004",
      "device":"iOS App",
      "token":"7665302325",
      "deviceAdded":"2016-06-14"
  },
  {
      "name":"Shelley Smith",
      "photo":"/img/sample/img-user-female.png",
      "role":"CTO",
      "email":"shelley.smith@a2finance.com",
      "group":"Administrators",
      "lastActive":"2017-05-01T21:40:30Z",
      "phone":"+44 779 206 2222",
      "location":"London, UK",
      "lng":"-0.057241",
      "lat":"51.543022",
      "device":"iOS App",
      "token":"2382472055",
      "deviceAdded":"2016-08-10"
  },
  {
      "name":"Tom Burk",
      "photo":"/img/sample/img-user.png",
      "role":"Service Administrator",
      "email":"tomburk@anothertestuser.co",
      "group":"Administrators",
      "lastActive":"2017-05-14T21:40:30Z",
      "phone":"+44 779 888 3244",
      "location":"London, UK",
      "lng":"-0.057241",
      "lat":"51.543022",
      "device":"Android App",
      "token":"6384472055",
      "deviceAdded":"2016-12-28"
  },
  {
      "name":"Jannette Taylor",
      "photo":"/img/sample/img-user-female.png",
      "role":"Bank clerk",
      "email":"jtaylor@somethinglikeyahoo.co",
      "group":"Users",
      "lastActive":"2016-11-14T21:40:30Z",
      "phone":"+1 815-459-4231",
      "location":"Ohio, USA",
      "lng":"-93.955078",
      "lat":"45.213004",
      "device":"iOS App",
      "token":"3754472055",
      "deviceAdded":"2016-11-14"
  }
]

export default App;

