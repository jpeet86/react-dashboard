import React from 'react';
import ReactDOM from 'react-dom';
import GetUsers from './App';
import './index.css';

ReactDOM.render(
  <GetUsers />,
  document.getElementById('root')
);
