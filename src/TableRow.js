import React, { Component } from 'react';
import Cell from './Cell.js';
import GoogleMap from './GoogleMap.js';

class TableRow extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isChecked: false,
      collapseState: "",
      linkClasses: "column-last clickable collapsed"
    }
     
  }

  handleChange() {
    this.props.checkSingle(this.props.index)
  }

  componentDidMount() {

    this.setState({
      collapseState: "collapse"
    })

  }

  toggleCollapse() {
    const currState = !this.state.collapseState;
    // const collapseState = currState ? "collapse" : "";
    let collapseState = "";
    let collapseLinkState = "";

    if (currState == false) {
      collapseState = "";
      collapseLinkState = "column-last clickable";

    } else {
      collapseState = "collapse";
      collapseLinkState = "column-last clickable collapsed";
    }

    this.setState({
      collapseState: collapseState,
      linkClasses: collapseLinkState
    })
  }

  render() {

    return (
      <div className="t-row">
        <div className="t-row-header">
            <div className="column-first"><input type="checkbox" onClick={this.handleChange.bind(this)} defaultChecked={this.props.checkedState} /></div>
            <Cell content={this.props.userEmail} />
            <Cell content={this.props.userGroup} />
            <Cell content={this.props.userLastActive} />
            <div className={this.state.linkClasses} onClick={this.toggleCollapse.bind(this)}><span className="link-accordion"> > </span></div>
        </div>
        <div className={this.state.collapseState}>
          <article className="user-details clearfix">
            <div className="user-content col-md-6 nopadding">

              <header>
                <h3 className="user-name">{this.props.userName}</h3>
                <h4 className="user-title">{this.props.userRole}</h4>
                <div className="user-image">
                  <img src={this.props.userPhoto} alt="{this.props.userName}" width="138" height="138" />
                </div>
              </header>

              <div className="user-devices">
                <p className="text-uppercase"><small>Auth devices</small></p>
                <div className="col-xs-6 nopadding">
                  <p><strong>{this.props.userDevice}</strong></p>
                  <p className="device"><i className="device-icon device-icon-ios"></i>{this.props.userDeviceAdded}</p>
                </div>
                <div className="col-xs-6 nopadding">
                  <p><strong>Harware Token</strong></p>
                  <p className="device"><i className="device-icon device-icon-token"></i>{this.props.userToken}</p>
                </div>

              </div>

              <div className="col-md-12">
                <hr className="hr"></hr>
              </div>

              <div className="col-md-12">
                <div className="user-details">
                  <p className="text-uppercase"><small>User details</small></p>
                  <dl className="list-split">
                    <dt>Email</dt>
                    <dd><a href="mailto:jackie.stewart@a2finance.com">{this.props.userEmail}</a></dd>
                    <dt>Phone number</dt>
                    <dd>{this.props.userPhone}</dd>
                    <dt>Role</dt>
                    <dd>{this.props.userRole}</dd>
                    <dt>Current location</dt>
                    <dd>{this.props.userLocation}</dd>
                  </dl>
                </div>
              </div>

            </div>

            {
              // Render Map component
            }
            <GoogleMap 
              mapLat={this.props.mapLat}
              mapLng={this.props.mapLng} />
          </article>
        </div>
      </div>
    );
  }
}

export default TableRow;
