import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const Marker = ({ text }) => <div className="marker">{text}</div>;

class GoogleMap extends Component {

	render() {

		return(
			<div className="map">
			  <GoogleMapReact
			      center={[parseFloat(this.props.mapLat),parseFloat(this.props.mapLng - 0.1)]}
			      defaultZoom={11}
			    >
			    <Marker
			      lat={this.props.mapLat}
			      lng={this.props.mapLng}
			      text={'IM HERE'}
			    />

			  </GoogleMapReact>
			</div>
		)
	}
}

export default GoogleMap;